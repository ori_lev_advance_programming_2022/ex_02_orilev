#ifndef NUCLEUS_H
#define NUCLEUS_H


#include <iostream>
#include <string>
#include <map>
#include <algorithm>

#define CODON_LENGHT 3

class Gene {
public:
	// init function
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	// Getter's
	unsigned int getStart() const;
	unsigned int getEnd() const;
	bool is_on_complementary_dna_strand() const;

	// Setter's
	void set_start(unsigned int start);
	void set_end(unsigned int end);
	// no setter to _on_complementary_dna_strand


private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};


class Nucleus
{
public:
	// init function
	void init(const std::string dna_sequence);

	// Getter's
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

private:
	// Capital letters?
	// i copied it from the pdf...
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

	std::map <char, char> convert_dna;
};


#endif // !NUCLEUS_H