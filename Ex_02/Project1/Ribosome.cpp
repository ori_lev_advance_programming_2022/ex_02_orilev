#include "Ribosome.h"

#define CODIN_LEN 3

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	std::string temp = "";
	Protein* p = new Protein;
	int i = 0;

	p->init();

	while(RNA_transcript.length() - i > CODIN_LEN - 1)
	{
		temp = RNA_transcript.substr(i, CODIN_LEN);
		i += CODIN_LEN;

		AminoAcid aminoacid = get_amino_acid(temp);

		p->add(aminoacid);

		if (aminoacid == UNKNOWN)
		{
			p->clear();
			delete p;
			return nullptr;
		}
	}

	return p;
}
