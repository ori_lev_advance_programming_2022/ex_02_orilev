#include "Cell.h"

#define MIN_GLUCOSE_LEVEL 50
#define REQURIED_ATP 100

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_atp_units = 0;
}


bool Cell::get_ATP()
{
	std::string RNA_transcript_copy = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* p = this->_ribosome.create_protein(RNA_transcript_copy);

	if (p == nullptr)
	{
		std::cerr << "Invalid glocus receptor gene" << std::endl;
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*(p));
	this->_mitochondrion.set_glucose(MIN_GLUCOSE_LEVEL);

	delete p;

	if(this->_mitochondrion.produceATP())
	{
		this->_atp_units = REQURIED_ATP;
		return true;
	}
	return false;



}

