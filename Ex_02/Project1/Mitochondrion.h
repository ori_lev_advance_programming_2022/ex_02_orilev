#ifndef MITOCHONDRION_H
#define MITOCHONDRION_H


#include <iostream>
#include <string>
#include "Protein.h"

class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;

private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};


#endif // !MITOCHONDRION_H
