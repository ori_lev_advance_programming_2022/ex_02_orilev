#include "Nucleus.h"


void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	// don't need to check if start\end is less then 0
	// they are unsigned....

	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// Getter's
unsigned int Gene::getStart() const { return this->_start; }
unsigned int Gene::getEnd() const { return this->_end; }
bool Gene::is_on_complementary_dna_strand() const { return this->_on_complementary_dna_strand; }

// Gene Setter's
void Gene::set_start(unsigned int start) { this->_start = start; }
void Gene::set_end(unsigned int end) { this->_end = end; }



void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";
	this->convert_dna = {
		{'A', 'T'},
		{'G', 'C'},
		{'C', 'G'},
		{'T', 'A'}
	};

	// run through the seq
	for (std::size_t i = 0; i < this->_DNA_strand.length(); ++i)
	{
		// check if there are good peace
		if (this->convert_dna.find(this->_DNA_strand[i]) != this->convert_dna.end())
		{
			// convert it and add it to the full DNA
			this->_complementary_DNA_strand += this->convert_dna[this->_DNA_strand[i]];
		}

		// exit
		else 
		{
			std::cerr << "DNA SEQ IS NOT GOOD (SHOULD CONTAINES ONLY A, G, C, T)!" << std::endl;
			_exit(1);
		}
	}
}


unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int counter = 0;
	if (codon.length() != CODON_LENGHT) { return counter; }

	// run through all the string -  2
	for (std::size_t i = 0; i < this->_DNA_strand.length() - 2; i++)
	{
		// check if there is a codon
		if((_DNA_strand[i] == codon[0]) && (_DNA_strand[i + 1] == codon[1]) && (_DNA_strand[i + 2] == codon[2]))
		{
			counter++;
		}
	}
	return counter;
}


std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	// new RNA to return
	std::string new_RNA_transcript = "";

	// check witch dna to copy
	//if (gene.is_on_complementary_dna_strand())
	//	const std::string & new_dna_to_return = this->_complementary_DNA_strand;
	//else
	//	const std::string& new_dna_to_return = this->_DNA_strand;
	const std::string& dna_to_convert = gene.is_on_complementary_dna_strand() ? _complementary_DNA_strand : _DNA_strand;


	for (std::size_t i = gene.getStart(); i <= gene.getEnd(); ++i)
	{
		if (dna_to_convert[i] == 'T') new_RNA_transcript += 'U';
		else new_RNA_transcript += dna_to_convert[i];
	}

	return new_RNA_transcript;
}


std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string to_rev = _DNA_strand;
	reverse(to_rev.begin(), to_rev.end());
	return to_rev;

}
