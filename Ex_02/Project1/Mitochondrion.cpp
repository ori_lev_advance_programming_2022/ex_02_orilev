#include "Mitochondrion.h"
#include "AminoAcid.h"

#define INSERT_GLUCOSE_RECEPTOR_LEN 7
#define MIN_GLOCUSE_LEVEL 50


void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}


void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	int i = 0;
	AminoAcid requried_acid[] = {ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END};

	AminoAcidNode* head = protein.get_first();
	for (std::size_t i = 0; i < INSERT_GLUCOSE_RECEPTOR_LEN; i++)
	{
		if (head == NULL) { return; }
		// check if continue
		if (head->get_data() != requried_acid[i]) { return; }
		head = head->get_next();
	}

	this->_has_glocuse_receptor = true;
}



void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}


bool Mitochondrion::produceATP() const
{
	return (this->_has_glocuse_receptor && (this->_glocuse_level >= MIN_GLOCUSE_LEVEL));
}


